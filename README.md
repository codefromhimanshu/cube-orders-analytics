# Cube Orders Dashboard

This applications provides analysis of users, orders, shipments, payments and revenue. The app includes user authentication with signup and login functionality, as well as protected routes that require the user to be logged in.

## Features

- User Authentication (Login/Signup)
- Revenue analysis
- Forward Renenue projection
- Sales, Payment birfurcation & failure analysis 
- Growth, Users, Product category analysis
- Responsive Design with Recharts Tailwind CSS

## Tech Stack

- **Next.js**: The React framework for production
- **Tailwind CSS**: A utility-first CSS framework for rapid UI development
- **NextAuth.js**: Authentication for Next.js
- **bcryptjs**: Library for hashing and checking passwords
- **Sequelize**: Next-generation ORM for Node.js
- **Typescript**: For strict type checking
- **Recharts**: For responsive charts
- **Cube**: React Cubejs package to interact with cubejs

## Getting Started

First, install the dependencies:

````bash
npm install
````
Copy .env.example to .env and change variable values 

````bash
cp .env.example .env
````
To run migration
````bash
./node_modules/sequelize-cli/lib/sequelize db:migrate
````
To seed data
````bash
./node_modules/sequelize-cli/lib/sequelize db:seed:all
````
To run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
````

 



## TODO
- Add Test cases
- CI/CD
- Use React Context for cubejs client
- Breakdown components further
- Separate interfaces & reuse them
- Restructure code
- Remove moment & numeral library and write own functions
- Move cubejs api after auth middleware
- Make auth more robust

