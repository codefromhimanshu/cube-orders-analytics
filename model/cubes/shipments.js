cube(`shipments`, {
  sql_table: `public.shipments`,

  data_source: `default`,

  joins: {
    orders: {
      sql: `${CUBE}.order_id = ${orders}.id`,
      relationship: `many_to_one`,
    },
  },

  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primary_key: true,
    },

    tracking_number: {
      sql: `tracking_number`,
      type: `string`,
    },

    shipment_status: {
      sql: `shipment_status`,
      type: `string`,
    },

    created_at: {
      sql: `created_at`,
      type: `time`,
    },

    updated_at: {
      sql: `updated_at`,
      type: `time`,
    },

    shipmentdate: {
      sql: `shipmentDate`,
      type: `time`,
    },
  },

  measures: {
    count: {
      type: `count`,
    },
    last_quarter: {
      sql: `id`,
      type: `count`,
      filters: [
        { sql: `${CUBE}.created_at > DATE_TRUNC('quarter', CURRENT_DATE)  ` },
      ],
    },
    last_to_last_quarter: {
      sql: `id`,
      type: `count`,
      filters: [
        {
          sql: `${CUBE}.created_at > (DATE_TRUNC('quarter', CURRENT_DATE) - '90 day'::INTERVAL) AND ${CUBE}.created_at < DATE_TRUNC('quarter', CURRENT_DATE)`,
        },
      ],
    },
    percentage_change_from_last_quarter: {
      type: `number`,
      format: `percent`,
      sql: `
        CASE WHEN ${last_to_last_quarter} = 0 THEN 0
        ELSE (${last_quarter} - ${last_to_last_quarter}) / ${last_to_last_quarter} * 100
        END
      `,
    },
  },

  pre_aggregations: {
    // Pre-aggregation definitions go here.
    // Learn more in the documentation: https://cube.dev/docs/caching/pre-aggregations/getting-started
  },
});
