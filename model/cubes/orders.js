cube(`orders`, {
  sql_table: `public.orders`,

  data_source: `default`,

  joins: {
    users: {
      sql: `${CUBE}.user_id = ${users}.id`,
      relationship: `many_to_one`,
    },
  },

  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primary_key: true,
    },

    total_amount: {
      sql: `total_amount`,
      type: `string`,
    },

    tax_value: {
      sql: `tax_value`,
      type: `string`,
    },

    status: {
      sql: `status`,
      type: `string`,
    },

    payment_status: {
      sql: `payment_status`,
      type: `string`,
    },

    created_at: {
      sql: `created_at`,
      type: `time`,
    },

    updated_at: {
      sql: `updated_at`,
      type: `time`,
    },
  },

  measures: {
    count: {
      type: `count`,
    },
    completed_orders_count: {
      type: `count`,
      filters: [{ sql: `${CUBE}.status = 'completed'` }],
    },
    total_revenue: {
      sql: `total_amount`,
      type: `sum`,
    },
    last_quarter_amount: {
      sql: `total_amount`,
      type: `sum`,
      filters: [
        { sql: `${CUBE}.created_at > DATE_TRUNC('quarter', CURRENT_DATE)  ` },
      ],
    },
    last_to_last_quarter_amount: {
      sql: `total_amount`,
      type: `sum`,
      filters: [
        {
          sql: `${CUBE}.created_at > (DATE_TRUNC('quarter', CURRENT_DATE) - '90 day'::INTERVAL) AND ${CUBE}.created_at < DATE_TRUNC('quarter', CURRENT_DATE)`,
        },
      ],
    },

    percentage_change_from_last_quarter: {
      type: `number`,
      format: `percent`,
      sql: `
        CASE WHEN ${last_to_last_quarter_amount} = 0 THEN 0
        ELSE (${last_quarter_amount} - ${last_to_last_quarter_amount}) / ${last_to_last_quarter_amount} * 100
        END
      `,
    },
  },

  pre_aggregations: {
    // Pre-aggregation definitions go here.
    // Learn more in the documentation: https://cube.dev/docs/caching/pre-aggregations/getting-started
  },
});
