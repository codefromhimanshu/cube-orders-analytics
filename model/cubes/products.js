cube(`products`, {
  sql_table: `public.products`,

  data_source: `default`,

  joins: {
    categories: {
      sql: `${CUBE}.category_id = ${categories}.id`,
      relationship: `belongsTo`,
    },
  },

  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primary_key: true,
    },

    price: {
      sql: `price`,
      type: `string`,
    },

    images: {
      sql: `images`,
      type: `string`,
    },

    name: {
      sql: `name`,
      type: `string`,
    },

    description: {
      sql: `description`,
      type: `string`,
    },

    created_at: {
      sql: `created_at`,
      type: `time`,
    },

    updated_at: {
      sql: `updated_at`,
      type: `time`,
    },
  },

  measures: {
    count: {
      type: `count`,
    },
  },

  pre_aggregations: {
    // Pre-aggregation definitions go here.
    // Learn more in the documentation: https://cube.dev/docs/caching/pre-aggregations/getting-started
  },
});
