cube(`product_variants`, {
  sql_table: `public.product_variants`,

  data_source: `default`,

  joins: {
    products: {
      sql: `${CUBE}.product_id = ${products}.id`,
      relationship: `many_to_one`,
    },
  },

  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primary_key: true,
    },

    variant_name: {
      sql: `variant_name`,
      type: `string`,
    },

    variant_value: {
      sql: `variant_value`,
      type: `string`,
    },

    created_at: {
      sql: `created_at`,
      type: `time`,
    },

    updated_at: {
      sql: `updated_at`,
      type: `time`,
    },
  },

  measures: {
    count: {
      type: `count`,
    },
  },

  pre_aggregations: {
    // Pre-aggregation definitions go here.
    // Learn more in the documentation: https://cube.dev/docs/caching/pre-aggregations/getting-started
  },
});
