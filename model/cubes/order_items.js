cube(`order_items`, {
  sql_table: `public.order_items`,

  data_source: `default`,

  joins: {
    orders: {
      sql: `${CUBE}.order_id = ${orders}.id`,
      relationship: `belongsTo`,
    },

    products: {
      sql: `${CUBE}.product_id = ${products}.id`,
      relationship: `belongsTo`,
    },

    product_variants: {
      sql: `${CUBE}.product_variant_id = ${product_variants}.id`,
      relationship: `belongsTo`,
    },
  },

  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primary_key: true,
    },

    price: {
      sql: `price`,
      type: `string`,
    },

    created_at: {
      sql: `created_at`,
      type: `time`,
    },

    updated_at: {
      sql: `updated_at`,
      type: `time`,
    },
  },

  measures: {
    count: {
      type: `count`,
    },

    quantity: {
      sql: `quantity`,
      type: `sum`,
    },
  },

  pre_aggregations: {
    // Pre-aggregation definitions go here.
    // Learn more in the documentation: https://cube.dev/docs/caching/pre-aggregations/getting-started
  },
});
