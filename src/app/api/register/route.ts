// export const dynamic = 'force-dynamic' // defaults to force-static
import bcrypt from "bcryptjs";
import { type NextRequest } from "next/server";

import { User } from "../../../models";

export async function POST(request: NextRequest) {
  try {
    const { email, password, name } = await request.json();
    const salt = bcrypt.genSaltSync(10);
    const user = await User.create({
      email,
      password: bcrypt.hashSync(password, salt),
      name,
    });
    return Response.json(
      { email: user.email },
      {
        status: 201,
      },
    );
  } catch (error) {
    return Response.json(
      { message: "Internal Server Error" },
      {
        status: 500,
      },
    );
  }
}
