"use client";
import React, { useState } from "react";

import { useSession } from "next-auth/react";

import CompletedOrdersChart from "../../components/CompletedOrdersChart";
import DateRangeFilter from "../../components/DateRangeFilter";
import InfoBullets from "../../components/InfoBullets";
import OrdersByPriceTier from "../../components/OrdersByPriceTier";
import OrdersOverTime from "../../components/OrdersOverTime";
import ProductsByCategoryChart from "../../components/ProductsByCategoryChart";
import RevenueGrowth from "../../components/RevenueGrowth";
import RevenueProjectionContainer from "../../components/RevenueProjectionContainer";
import UsersOverTime from "../../components/UsersOverTime";
import VerticalBullets from "../../components/VerticalBullets";
import cubejsApi from "../cubejs-client";

const DashboardPage: React.FC = () => {
  const { data: session, status } = useSession();

  const [dateRange, setDateRange] = useState({
    startDate: new Date("2022-01-01"),
    endDate: new Date("2023-12-31"),
  });
  console.log(session);
  if (!session) {
    window.location.href = "/login";
  }

  const handleDateChange = (range) => {
    setDateRange(range);
  };
  return (
    <div className="container mx-auto p-4">
      <InfoBullets cubejsApi={cubejsApi} />
      <div className="container mx-auto p-2  md:p-5">
        <CompletedOrdersChart cubejsApi={cubejsApi} />
      </div>
      <div className="container mx-auto p-2  md:p-5">
        <ProductsByCategoryChart cubejsApi={cubejsApi} />
      </div>
      <div className="container mx-auto justify-around p-2 md:p-5 flex flex-wrap ">
        <OrdersByPriceTier cubejsApi={cubejsApi} />
        <VerticalBullets cubejsApi={cubejsApi} />
        <DateRangeFilter
          start={dateRange.startDate}
          end={dateRange.endDate}
          onDateChange={handleDateChange}
        />
      </div>

      <div className="container flex flex-wrap mx-auto p-2  md:p-5">
        <RevenueGrowth cubejsApi={cubejsApi} dateRange={dateRange} />
        <RevenueProjectionContainer
          cubejsApi={cubejsApi}
          dateRange={dateRange}
        />
      </div>

      <div className="flex flex-wrap -mx-3 p-2 ">
        <UsersOverTime cubejsApi={cubejsApi} dateRange={dateRange} />
        <OrdersOverTime cubejsApi={cubejsApi} dateRange={dateRange} />
      </div>
    </div>
  );
};

export default DashboardPage;
