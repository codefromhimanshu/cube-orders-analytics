import React from "react";

import { getServerSession } from "next-auth/next";

import SignOut from "../components/Logout";
import NextAuthProvider from "../components/NextAuthProvider";
import { authOptions } from "../utils/auth-options";

import type { Metadata } from "next";

import "./globals.css";

export const metadata: Metadata = {
  title: "Cube Orders Dashboard",
  description: "Cube Orders Dashboard",
};

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const session = await getServerSession(authOptions);
  return (
    <html lang="en">
      <body>
        <NextAuthProvider session={session}>
          <div className="min-h-screen bg-gray-100 ">
            <div className="bg-white px-4 py-5 shadow-md">
              <div className="flex justify-between items-center container mx-auto px-10">
                <h1 className="text-xl font-bold">Orders Analytics</h1>
                <SignOut />
              </div>
            </div>
            <div className=" mx-auto ">{children}</div>
          </div>
        </NextAuthProvider>
      </body>
    </html>
  );
}
