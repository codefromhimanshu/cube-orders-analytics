import { redirect } from "next/navigation";
import { getServerSession } from "next-auth/next";

import { authOptions } from "../utils/auth-options";

const Home = async (params) => {
  const session = await getServerSession(authOptions);
  if (!session) {
    redirect("/login");
  } else {
    redirect("/dashboard");
  }
};

export default Home;
