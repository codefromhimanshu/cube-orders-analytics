import moment from "moment";
import numeral from "numeral";
const numberFormatter = (item) => numeral(item).format("0,0");

const dollarFormatter = (item) => `$${numeral(item).format("0,0")}`;
const monthFormatter = (item) => {
  return moment(item).format("MMM YY");
};

const dateFormatter = (item) => {
  return moment(item).format("DD MMM");
};

const dateRangeFormat = (dateRange) => {
  return [
    moment(dateRange.startDate).format("YYYY-MM-DD"),
    moment(dateRange.endDate).format("YYYY-MM-DD"),
  ];
};

export {
  numberFormatter,
  dateFormatter,
  monthFormatter,
  dollarFormatter,
  dateRangeFormat,
};
