import React from "react";

import {
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
  AreaChart,
  Area,
} from "recharts";

import { numberFormatter, monthFormatter, dateRangeFormat } from "../utils";

import Chart from "./Chart";

interface UsersOverTimeProps {
  cubejsApi: any;
  dateRange: {
    startDate: Date;
    endDate: Date;
  };
}

const UsersOverTime: React.FC<UsersOverTimeProps> = ({
  cubejsApi,
  dateRange,
}) => {
  return (
    <div className="w-full sm:w-1/2 mb-6">
      <Chart
        cubejsApi={cubejsApi}
        title="New Users Over Time"
        query={{
          measures: ["users.count"],
          timeDimensions: [
            {
              dimension: "users.created_at",
              dateRange: dateRangeFormat(dateRange),
              granularity: "week",
            },
          ],
        }}
        render={(resultSet) => (
          <ResponsiveContainer width="100%" height={300}>
            <AreaChart data={resultSet.chartPivot()}>
              <XAxis dataKey="x" tickFormatter={monthFormatter} />
              <YAxis tickFormatter={numberFormatter} />
              <Tooltip labelFormatter={monthFormatter} />
              <Area
                type="monotone"
                dataKey="users.count"
                name="Users"
                stroke="rgb(106, 110, 229)"
                fill="rgba(106, 110, 229, .16)"
              />
            </AreaChart>
          </ResponsiveContainer>
        )}
      />
    </div>
  );
};

export default UsersOverTime;
