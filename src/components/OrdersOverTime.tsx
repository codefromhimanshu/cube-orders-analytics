import React from "react";

import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

import { numberFormatter, monthFormatter, dateRangeFormat } from "../utils";

import Chart from "./Chart";

interface OrdersOverTimeProps {
  cubejsApi: any;
  dateRange: {
    startDate: Date;
    endDate: Date;
  };
}

const OrdersOverTime: React.FC<OrdersOverTimeProps> = ({
  cubejsApi,
  dateRange,
}) => {
  return (
    <div className="w-full sm:w-1/2 mb-6">
      <Chart
        cubejsApi={cubejsApi}
        title="Orders by Status Over time"
        query={{
          measures: ["orders.count"],
          dimensions: ["orders.status"],
          timeDimensions: [
            {
              dimension: "orders.created_at",
              dateRange: dateRangeFormat(dateRange),
              granularity: "month",
            },
          ],
        }}
        render={(resultSet) => {
          return (
            <ResponsiveContainer width="100%" height={300}>
              <BarChart data={resultSet.chartPivot()}>
                <XAxis tickFormatter={monthFormatter} dataKey="x" />
                <YAxis tickFormatter={numberFormatter} />
                <Bar
                  stackId="a"
                  dataKey="shipped,orders.count"
                  name="Shipped"
                  fill="#7DB3FF"
                />
                <Bar
                  stackId="a"
                  dataKey="processing,orders.count"
                  name="Processing"
                  fill="#49457B"
                />
                <Bar
                  stackId="a"
                  dataKey="completed,orders.count"
                  name="Completed"
                  fill="#FF7C78"
                />
                <Legend />
                <Tooltip
                  // @ts-ignore
                  labelFormatter={(value, name, props) => [
                    monthFormatter(value),
                  ]}
                />
              </BarChart>
            </ResponsiveContainer>
          );
        }}
      />
    </div>
  );
};

export default OrdersOverTime;
