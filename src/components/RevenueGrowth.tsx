import React from "react";

import { QueryRenderer } from "@cubejs-client/react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

import { dollarFormatter, monthFormatter, dateRangeFormat } from "../utils";

interface RevenueGrowthProps {
  cubejsApi: any;
  dateRange: {
    startDate: Date;
    endDate: Date;
  };
}

const RevenueGrowth: React.FC<RevenueGrowthProps> = ({
  cubejsApi,
  dateRange,
}) => {
  return (
    <QueryRenderer
      query={{
        measures: ["orders.total_revenue"],
        timeDimensions: [
          {
            dimension: "orders.created_at",
            // @ts-ignore
            dateRange: dateRangeFormat(dateRange),
            granularity: "month",
          },
        ],
      }}
      cubejsApi={cubejsApi}
      render={({ resultSet }) => {
        if (!resultSet) {
          return <div>Loading...</div>;
        }

        return (
          <div className="p-4 border rounded shadow-md w-full sm:w-6/12 bg-white rounded-xl shadow-md overflow-hidden ">
            <div className="m-2">
              <div className="tracking-wide text-sm text-indigo-500 font-semibold my-2">
                Revenue Growth last year
              </div>
              <ResponsiveContainer width="100%" height={350}>
                <LineChart
                  margin={{ top: 5, left: 50 }}
                  data={resultSet.tablePivot()}
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis
                    tickFormatter={monthFormatter}
                    dataKey="orders.created_at.month"
                  />
                  <YAxis
                    orientation="left"
                    tickFormatter={dollarFormatter}
                    stroke="#82ca9d"
                  />
                  <Tooltip
                    // @ts-ignore
                    labelFormatter={(value, name, props) => [
                      monthFormatter(value),
                    ]}
                    formatter={(value, name, props) => [value, name]}
                  />
                  <Legend />
                  <Line
                    type="monotone"
                    name="Sold product's total revenue"
                    dataKey="orders.total_revenue"
                    stroke="#82ca9d"
                    activeDot={{ r: 10 }}
                    // @ts-ignore
                    label={dollarFormatter}
                  />
                </LineChart>
              </ResponsiveContainer>
            </div>
          </div>
        );
      }}
    />
  );
};

export default RevenueGrowth;
