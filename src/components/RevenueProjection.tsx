import React from "react";

import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

import { dollarFormatter } from "../utils";

interface RevenueData {
  name: string;
  revenue: number;
}

interface RevenueProjectionProps {
  currentRevenue: RevenueData[];
  projectedRevenue: RevenueData[];
}

const RevenueProjection: React.FC<RevenueProjectionProps> = ({
  currentRevenue,
  projectedRevenue,
}) => (
  <div className="p-4 border rounded shadow-md w-full sm:w-6/12 bg-white rounded-xl shadow-md overflow-hidden ">
    <div className="m-2">
      <div className="tracking-wide text-sm text-indigo-500 font-semibold my-2">
        Revenue Projection
      </div>
      <ResponsiveContainer width="100%" height={350}>
        <LineChart
          margin={{ top: 5, left: 50, bottom: 0, right: 0 }}
          data={[...currentRevenue, ...projectedRevenue]}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis
            orientation="left"
            tickFormatter={dollarFormatter}
            stroke="#8884d8"
          />
          <Tooltip />
          <Legend />
          <Line
            type="monotone"
            dataKey="revenue"
            stroke="#8884d8"
            name="Forward Revenue"
            activeDot={{ r: 8 }}
            // @ts-ignore
            label={dollarFormatter}
          />
        </LineChart>
      </ResponsiveContainer>
    </div>
  </div>
);

export default RevenueProjection;
