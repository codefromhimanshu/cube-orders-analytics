// components/CompletedOrdersChart.tsx
import React from "react";

import { QueryRenderer } from "@cubejs-client/react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

import { dateFormatter } from "../utils";

const CompletedOrdersChart = ({ cubejsApi }) => {
  return (
    <div className="p-4 bg-white rounded-lg shadow">
      <div className="tracking-wide text-sm text-indigo-500 font-semibold my-2">
        Completed Orders Last 30 days
      </div>
      <QueryRenderer
        query={{
          measures: ["orders.completed_orders_count"],
          timeDimensions: [
            {
              dimension: "orders.created_at",
              dateRange: "last 30 days",
              granularity: "day",
            },
          ],
        }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div>Loading...</div>;
          }
          const data = resultSet.chartPivot();
          return (
            <ResponsiveContainer width="100%" height={300}>
              <LineChart data={data}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="x" tickFormatter={dateFormatter} />
                <YAxis allowDecimals={false} stroke="#3182CE" />
                <Tooltip
                  // @ts-ignore
                  labelFormatter={(value, name, props) => [
                    dateFormatter(value),
                  ]}
                  formatter={(value, name, props) => [value, name]}
                />
                <Legend />
                <Line
                  type="monotone"
                  dataKey="orders.completed_orders_count"
                  stroke="#3182CE"
                  name="Orders Count"
                  activeDot={{ r: 8 }}
                />
              </LineChart>
            </ResponsiveContainer>
          );
        }}
      />
    </div>
  );
};

export default CompletedOrdersChart;
