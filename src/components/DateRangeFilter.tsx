import React, { useState } from "react";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

interface DateRangeFilterProps {
  start: Date;
  end: Date;
  onDateChange: ({
    startDate,
    endDate,
  }: {
    startDate: Date;
    endDate: Date;
  }) => void;
}
const DateRangeFilter: React.FC<DateRangeFilterProps> = ({
  start,
  end,
  onDateChange,
}) => {
  const [startDate, setStartDate] = useState(start);
  const [endDate, setEndDate] = useState(end);

  const handleDateChange = (dates) => {
    const [start, end] = dates;
    setStartDate(start);
    setEndDate(end);
    onDateChange({ startDate: start, endDate: end });
  };

  return (
    <div className="float-right mb-6">
      <div className="tracking-wide text-sm text-indigo-500 font-semibold my-2">
        Choose Date range to filter below charts
      </div>
      <div className="float-right">
        <DatePicker
          selected={startDate}
          onChange={handleDateChange}
          startDate={startDate}
          endDate={endDate}
          selectsRange
          inline
        />
      </div>

      {/* <button
        className="bg-blue-500 text-white active:bg-blue-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 display-block w-1/2 align-right float-right"
        type="button"
        onClick={() => onDateChange({ startDate, endDate })}
      >
        Apply
      </button> */}
    </div>
  );
};

export default DateRangeFilter;
