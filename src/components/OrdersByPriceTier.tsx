import React from "react";

import { QueryRenderer } from "@cubejs-client/react";
import {
  PieChart,
  Pie,
  Tooltip,
  Legend,
  ResponsiveContainer,
  Cell,
} from "recharts";

interface PieChartData {
  name: string;
  value: number;
}

const COLORS = [
  "#1f77b4", // muted blue
  "#ff7f0e", // safety orange
  "#2ca02c", // cooked asparagus green
  "#d62728", // brick red
  "#9467bd", // muted purple
  "#8c564b", // chestnut brown
  "#e377c2", // raspberry yogurt pink
  "#7f7f7f", // middle gray
  "#bcbd22", // curry yellow-green
  "#17becf", // blue-teal
  "#aec7e8", // soft blue
  "#ffbb78", // soft orange
  "#98df8a", // pale green
  "#ff9896", // soft red
  "#c5b0d5", // soft purple
  "#c49c94", // brownish pink
  "#f7b6d2", // soft pink
  "#c7c7c7", // light gray
  "#dbdb8d", // dirty yellow
  "#9edae5", // soft teal
];
const pieChartQuery = {
  measures: ["orders.count"],
  dimensions: ["orders.total_amount"],
};
function sumValuesByRange(data) {
  const findRange = (value) => Math.floor(value / 2000) * 2000;

  const rangeSums = {};
  data.forEach((item) => {
    const value = parseFloat(item.x);

    const rangeKey = findRange(value).toString();
    if (!rangeSums[rangeKey]) {
      rangeSums[rangeKey] = 0;
    }
    rangeSums[rangeKey] += item["orders.count"];
  });

  return Object.entries(rangeSums).map(([range, sum]) => ({
    name: `${range}-${parseInt(range) + 1999}`,
    value: sum,
  }));
}
const OrdersByPriceTier: React.FC<{ cubejsApi: any }> = ({ cubejsApi }) => {
  return (
    <QueryRenderer
      query={pieChartQuery}
      cubejsApi={cubejsApi}
      render={({ resultSet }) => {
        if (!resultSet) {
          return <div>Loading...</div>;
        }
        const data = sumValuesByRange(resultSet.chartPivot());
        return (
          <div className="p-4 bg-white rounded-md shadow-lg w-full sm:w-4/12 mb-6">
            <div className="tracking-wide text-sm text-indigo-500 font-semibold my-2">
              Orders by Price Tiers
            </div>

            <ResponsiveContainer width="100%" height={300}>
              <PieChart>
                <Pie
                  data={data}
                  cx="50%"
                  cy="50%"
                  outerRadius={100}
                  fill="#8884d8"
                  dataKey="value"
                  label={(entry: PieChartData) => `$${entry.value}`}
                >
                  {data.map((entry, index) => (
                    <Cell
                      key={`cell-${index}`}
                      fill={COLORS[index % COLORS.length]}
                    />
                  ))}
                </Pie>
                <Tooltip />
                <Legend />
              </PieChart>
            </ResponsiveContainer>
          </div>
        );
      }}
    />
  );
};

export default OrdersByPriceTier;
