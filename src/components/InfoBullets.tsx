import React from "react";

import { QueryRenderer } from "@cubejs-client/react";

import { numberFormatter } from "../utils";

interface InfoBulletsProps {
  cubejsApi: any; // You should use the actual Cube.js API client type
}

const renderSingleValue = (resultSet, key) => (
  <>{numberFormatter(resultSet.chartPivot()[0][key])}</>
);

const InfoBullets: React.FC<InfoBulletsProps> = ({ cubejsApi }) => {
  return (
    <div className="flex justify-around flex-wrap gap-2 md:gap-4 my-4 mx-4">
      <QueryRenderer
        query={{ measures: ["users.count"] }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div className="loader">Loading...</div>;
          }
          return (
            <div className="w-5/12 md:w-1/6 bg-gradient-to-r from-purple-400 to-purple-300 p-4 rounded-lg shadow-md">
              <p className="text-sm text-purple-700">Total Users</p>
              <p className="text-xl font-bold text-white">
                {renderSingleValue(resultSet, "users.count")}
              </p>
            </div>
          );
        }}
      />
      <QueryRenderer
        query={{ measures: ["orders.count"] }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div className="loader">Loading...</div>;
          }
          return (
            <div className="w-5/12  md:w-1/6 bg-gradient-to-r from-orange-400 to-orange-300 p-4 rounded-lg shadow-md">
              <p className="text-sm text-orange-700">Total Orders</p>
              <p className="text-xl font-bold text-white">
                {renderSingleValue(resultSet, "orders.count")}
              </p>
            </div>
          );
        }}
      />

      <QueryRenderer
        query={{
          measures: ["shipments.count"],
          filters: [
            {
              dimension: "shipments.shipment_status",
              operator: "equals",
              values: ["shipped"],
            },
          ],
        }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div className="loader">Loading...</div>;
          }
          return (
            <div className="w-5/12  md:w-1/6 bg-gradient-to-r from-green-400 to-green-300 p-4 rounded-lg shadow-md">
              <p className="text-sm text-green-700">Shipped Orders</p>
              <p className="text-xl font-bold text-white">
                {renderSingleValue(resultSet, "shipments.count")}
              </p>
            </div>
          );
        }}
      />

      <QueryRenderer
        query={{
          measures: ["shipments.count"],
          filters: [
            {
              dimension: "shipments.shipment_status",
              operator: "equals",
              values: ["pending"],
            },
          ],
        }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div className="loader">Loading...</div>;
          }
          return (
            <div className="w-5/12 md:w-1/6 bg-gradient-to-r from-yellow-400 to-yellow-300 p-4 rounded-lg shadow-md">
              <p className="text-sm text-yellow-700">Pending Shipment</p>
              <p className="text-xl font-bold text-white">
                {renderSingleValue(resultSet, "shipments.count")}
              </p>
            </div>
          );
        }}
      />

      <QueryRenderer
        query={{
          measures: ["shipments.count"],
          filters: [
            {
              dimension: "shipments.shipment_status",
              operator: "equals",
              values: ["delivered"],
            },
          ],
        }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div className="loader">Loading...</div>;
          }
          return (
            <div className="w-5/12 md:w-1/6 bg-gradient-to-r from-blue-400 to-blue-300 p-4 rounded-lg shadow-md">
              <p className="text-sm text-blue-700">Delivered Orders</p>
              <p className="text-xl font-bold text-white">
                {renderSingleValue(resultSet, "shipments.count")}
              </p>
            </div>
          );
        }}
      />
    </div>
  );
};

export default InfoBullets;
