import React from "react";

import { ResultSet } from "@cubejs-client/core";
import { QueryRenderer } from "@cubejs-client/react";

// Defining the types for the props
interface ChartProps {
  cubejsApi: any; // Replace 'any' with the specific type of your cubejsApi, if known
  title: string;
  query: any; // Replace 'any' with the specific type of your query object, if known
  render: (resultSet: ResultSet) => JSX.Element;
}

const Chart: React.FC<ChartProps> = ({ cubejsApi, title, query, render }) => (
  // Using Tailwind CSS for styling
  <div className="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-2xl">
    <div className="">
      <div className="p-8">
        <div className="uppercase tracking-wide text-sm text-indigo-500 font-semibold">
          {title}
        </div>
        <div className="mt-2 text-gray-500">
          <QueryRenderer
            query={query}
            cubejsApi={cubejsApi}
            render={({ resultSet }) => {
              if (!resultSet) {
                return <div className="loader">Loading...</div>;
              }
              return render(resultSet);
            }}
          />
        </div>
      </div>
    </div>
    <div className="bg-gradient-to-r from-green-400 to-green-300 p-4 rounded-lg shadow-md">
      <p className="text-sm text-green-700"> {title}</p>
      <p className="text-xl font-bold text-white">1.03%</p>
      <p className="text-xs text-green-800">+32% above benchmark</p>
    </div>
  </div>
);

export default Chart;
