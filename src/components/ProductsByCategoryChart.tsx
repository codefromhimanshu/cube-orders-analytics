import React from "react";

import { QueryRenderer } from "@cubejs-client/react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

interface ProductsByCategoryChartProps {
  cubejsApi: any;
}

const renderLegend = (value: string, entry: any) => {
  const { color } = entry;

  return <span style={{ color }}>{value}</span>;
};

const ProductsByCategoryChart: React.FC<ProductsByCategoryChartProps> = ({
  cubejsApi,
}) => {
  return (
    <div className="p-4 bg-white rounded-md shadow-md">
      <div className="tracking-wide text-sm text-indigo-500 font-semibold my-2 my-2">
        Products Listed by Category Name
      </div>
      <QueryRenderer
        query={{
          measures: ["products.count"],
          dimensions: ["categories.name"],
        }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div>Loading...</div>;
          }

          const data = resultSet.chartPivot();
          return (
            <ResponsiveContainer width="100%" height={300}>
              <BarChart data={data}>
                <XAxis dataKey="x" />
                <YAxis />
                <Tooltip />
                <Legend formatter={renderLegend} align="center" />
                <Bar
                  dataKey="products.count"
                  name="Products"
                  fill="#3182CE"
                  legendType="line"
                />
              </BarChart>
            </ResponsiveContainer>
          );
        }}
      />
    </div>
  );
};

export default ProductsByCategoryChart;
