import React from "react";

import { ResultSet } from "@cubejs-client/core";
import { QueryRenderer } from "@cubejs-client/react";

// Defining the types for the props
interface ChartProps {
  cubejsApi: any; // Replace 'any' with the specific type of your cubejsApi, if known
  title: string;
  query: any; // Replace 'any' with the specific type of your query object, if known
  render: (resultSet: ResultSet) => JSX.Element;
}

const Chart: React.FC<ChartProps> = ({ cubejsApi, title, query, render }) => (
  // Using Tailwind CSS for styling
  <div className="p-4 max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-2xl">
    <div className="">
      <div className="p-2">
        <div className=" tracking-wide text-sm text-indigo-500 font-semibold my-2">
          {title}
        </div>
        <div className="mt-2 text-gray-500">
          <QueryRenderer
            query={query}
            cubejsApi={cubejsApi}
            render={({ resultSet }) => {
              if (!resultSet) {
                return <div className="loader">Loading...</div>;
              }
              return render(resultSet);
            }}
          />
        </div>
      </div>
    </div>
  </div>
);

export default Chart;
