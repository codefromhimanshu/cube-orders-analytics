import React from "react";

import { QueryRenderer } from "@cubejs-client/react";

import { monthFormatter, dateRangeFormat } from "../utils";

import RevenueProjection from "./RevenueProjection";

interface RevenueProjectionContainerProps {
  cubejsApi: any;
  dateRange: {
    startDate: Date;
    endDate: Date;
  };
}

const RevenueProjectionContainer: React.FC<RevenueProjectionContainerProps> = ({
  cubejsApi,
  dateRange,
}) => {
  return (
    <QueryRenderer
      query={{
        measures: ["orders.total_revenue"],
        timeDimensions: [
          {
            dimension: "orders.created_at",
            // @ts-ignore
            dateRange: dateRangeFormat(dateRange),
            granularity: "quarter",
          },
        ],
      }}
      cubejsApi={cubejsApi}
      render={({ resultSet }) => {
        if (!resultSet) {
          return <div>Loading...</div>;
        }
        const data = resultSet.tablePivot();

        let totalGrowthRate = 0;

        const currentRevenue = data.map((row, index) => {
          let growthRate: any | number | string = 0;
          if (index > 0) {
            const revenue = data[index - 1]["orders.total_revenue"];
            // @ts-ignore
            growthRate = (row["orders.total_revenue"] - revenue) / revenue;
          }

          totalGrowthRate += growthRate;
          return {
            name: `Q${monthFormatter(row["orders.created_at.quarter"])}`,
            // @ts-ignore
            revenue: Math.round(row["orders.total_revenue"] * 100) / 100,
          };
        });
        const averageGrowthRate = totalGrowthRate / (data.length - 1);

        let lastKnownRevenue = data[data.length - 1]["orders.total_revenue"];
        const projectedRevenues = [];
        const today = new Date();
        const quarter = Math.floor(today.getMonth() / 3);
        const startFullQuarter = new Date(today.getFullYear(), quarter * 3, 1);

        for (let i = 0; i < 3; i++) {
          // @ts-ignore
          const nextRevenue = lastKnownRevenue * (1 + averageGrowthRate);
          const endFullQuarter = new Date(
            startFullQuarter.getFullYear(),
            startFullQuarter.getMonth() + 3 * (i + 2),
            0,
          );
          projectedRevenues.push({
            name: `Q${monthFormatter(endFullQuarter)}`,
            revenue: Math.round(nextRevenue * 100) / 100,
          });
          lastKnownRevenue = nextRevenue;
        }

        return (
          <RevenueProjection
            currentRevenue={currentRevenue}
            projectedRevenue={projectedRevenues}
          />
        );
      }}
    />
  );
};

export default RevenueProjectionContainer;
