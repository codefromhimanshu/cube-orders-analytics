import React from "react";

import { QueryRenderer } from "@cubejs-client/react";

import { numberFormatter } from "../utils";

interface VerticalBulletsProps {
  cubejsApi: any; // You should use the actual Cube.js API client type
}

const renderSingleValue = (resultSet, key) => (
  <>{numberFormatter(resultSet.chartPivot()[0][key])}</>
);

const VerticalBullets: React.FC<VerticalBulletsProps> = ({ cubejsApi }) => {
  return (
    <div className="grid md:grid-cols-2 gap-2 md:gap-2  mx-4">
      <QueryRenderer
        query={{
          measures: ["payments.count"],
          filters: [
            {
              dimension: "payments.payment_status",
              operator: "equals",
              values: ["paid"],
            },
            {
              dimension: "payments.payment_method",
              operator: "equals",
              values: ["Credit Card"],
            },
          ],
        }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div className="loader">Loading...</div>;
          }
          return (
            <div className="w-full h-28 bg-gradient-to-r from-teal-400 to-cyan-500 p-4 rounded-lg shadow-md pt-8">
              <p className="text-sm text-cyan-950">Total Card Payments</p>
              <p className="text-xl font-bold text-white">
                {renderSingleValue(resultSet, "payments.count")}
              </p>
            </div>
          );
        }}
      />
      <QueryRenderer
        query={{
          measures: ["payments.count"],
          filters: [
            {
              dimension: "payments.payment_status",
              operator: "equals",
              values: ["paid"],
            },
            {
              dimension: "payments.payment_method",
              operator: "equals",
              values: ["Bank Transfer"],
            },
          ],
        }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div className="loader">Loading...</div>;
          }
          return (
            <div className="w-full h-28 bg-gradient-to-r from-pink-500 to-rose-400 p-4 rounded-lg shadow-md pt-8">
              <p className="text-sm text-rose-950">
                Total Bank Transfer Payments
              </p>
              <p className="text-xl font-bold text-white">
                {renderSingleValue(resultSet, "payments.count")}
              </p>
            </div>
          );
        }}
      />
      <QueryRenderer
        query={{
          measures: ["payments.count"],
          filters: [
            {
              dimension: "payments.payment_status",
              operator: "equals",
              values: ["paid"],
            },
            {
              dimension: "payments.payment_method",
              operator: "equals",
              values: ["Bank Transfer"],
            },
          ],
        }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div>Loading...</div>;
          }

          return (
            <div className="w-full h-28  bg-gradient-to-r from-lime-500 to-emerald-600 p-4 rounded-lg shadow-md pt-8">
              <p className="text-sm text-lime-950">Total PayPal Payments</p>
              <p className="text-xl font-bold text-white">
                {renderSingleValue(resultSet, "payments.count")}
              </p>
            </div>
          );
        }}
      />
      <QueryRenderer
        query={{
          measures: ["payments.count"],
          filters: [
            {
              dimension: "payments.payment_status",
              operator: "equals",
              values: ["failed", "pending"],
            },
          ],
        }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div>Loading...</div>;
          }

          return (
            <div className="w-full h-28  bg-gradient-to-r from-yellow-400 to-yellow-300 p-4 rounded-lg shadow-md pt-8">
              <p className="text-sm text-yellow-950">Failed/Pending Payments</p>
              <p className="text-xl font-bold text-white">
                {renderSingleValue(resultSet, "payments.count")}
              </p>
            </div>
          );
        }}
      />
      <QueryRenderer
        query={{ measures: ["order_items.quantity"] }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div className="loader">Loading...</div>;
          }
          return (
            <div className="w-full h-28 bg-gradient-to-r from-indigo-300 to-indigo-500 p-4 rounded-lg shadow-md pt-8">
              <p className="text-sm text-indigo-900">
                Quantity of products orderd
              </p>
              <p className="text-xl font-bold text-white">
                {renderSingleValue(resultSet, "order_items.quantity")}
              </p>
            </div>
          );
        }}
      />
      <QueryRenderer
        query={{
          measures: [
            "orders.total_revenue",
            "orders.percentage_change_from_last_quarter",
          ],
        }}
        cubejsApi={cubejsApi}
        render={({ resultSet }) => {
          if (!resultSet) {
            return <div>Loading...</div>;
          }
          const percent_change = numberFormatter(
            resultSet.chartPivot()[0][
              "orders.percentage_change_from_last_quarter"
            ],
          );
          return (
            <div className="w-full h-28 bg-gradient-to-r from-fuchsia-500 to-fuchsia-200 p-4 rounded-lg shadow-md pt-6">
              <p className="text-sm  text-fuchsia-900">Total Sales</p>
              <p className="text-xl font-bold text-white">
                ${renderSingleValue(resultSet, "orders.total_revenue")}
              </p>
              <p className="text-xs text-fuchsia-900">
                {percent_change}% {percent_change > 0 ? "above" : "below"} last
                quarter
              </p>
            </div>
          );
        }}
      />
    </div>
  );
};

export default VerticalBullets;
