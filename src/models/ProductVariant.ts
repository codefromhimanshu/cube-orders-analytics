import { Sequelize, DataTypes, Model, Optional } from "sequelize";

interface ProductVariantAttributes {
  id: number;
  productId: number;
  variantName: string;
  variantValue: string;
  createdAt: Date;
  updatedAt: Date;
}

interface ProductVariantCreationAttributes
  extends Optional<ProductVariantAttributes, "id"> {}

class ProductVariant
  extends Model<ProductVariantAttributes, ProductVariantCreationAttributes>
  implements ProductVariantAttributes
{
  public id!: number;
  public productId!: number;
  public variantName!: string;
  public variantValue!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export default function (sequelize: Sequelize): typeof ProductVariant {
  ProductVariant.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      productId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: { model: "products", key: "id" },
        field: "product_id",
      },
      variantName: {
        type: new DataTypes.STRING(128),
        allowNull: false,
        field: "variant_name",
      },
      variantValue: {
        type: new DataTypes.STRING(128),
        allowNull: false,
        field: "variant_value",
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
        field: "created_at",
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
        field: "updated_at",
      },
    },
    {
      tableName: "product_variants",
      sequelize,
    },
  );

  return ProductVariant;
}
