import { Sequelize, DataTypes, Model, Optional } from "sequelize";

interface InvoiceAttributes {
  id: number;
  orderId: number;
  issuedDate: Date;
  totalAmount: number;
  createdAt: Date;
  updatedAt: Date;
}

interface InvoiceCreationAttributes extends Optional<InvoiceAttributes, "id"> {}

class Invoice
  extends Model<InvoiceAttributes, InvoiceCreationAttributes>
  implements InvoiceAttributes
{
  public id!: number;
  public orderId!: number;
  public issuedDate!: Date;
  public totalAmount!: number;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export default function (sequelize: Sequelize): typeof Invoice {
  Invoice.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      orderId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: { model: "orders", key: "id" },
        field: "order_id",
      },
      issuedDate: {
        type: DataTypes.DATE,
        allowNull: false,
        field: "issued_date",
      },
      totalAmount: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false,
        field: "total_amount",
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
        field: "created_at",
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
        field: "updated_at",
      },
    },
    {
      tableName: "invoices",
      sequelize,
    },
  );

  return Invoice;
}
