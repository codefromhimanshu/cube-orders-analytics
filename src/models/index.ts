import * as pg from "pg";
import { Sequelize } from "sequelize";
import { Dialect } from "sequelize";

import InvoiceModel from "./Invoice";
import OrderModel from "./Order";
import OrderItemModel from "./OrderItem";
import PaymentModel from "./Payment";
import ProductModel from "./Product";
import ProductVariantModel from "./ProductVariant";
import ShipmentModel from "./Shipment";
import UserModel from "./User";
require("dotenv").config();

// Use environment variables or hard code your database credentials
const dbConfig = {
  database: process.env.DB_NAME || "database",
  username: process.env.DB_USER || "username",
  password: process.env.DB_PASS || "password",
  host: process.env.DB_HOST || "localhost",
  dialect: "postgres" as Dialect,
};

const sequelize = new Sequelize(
  dbConfig.database,
  dbConfig.username,
  dbConfig.password,
  {
    host: dbConfig.host,
    dialectModule: pg,
    dialect: dbConfig.dialect,
  },
);

// Load models
const User = UserModel(sequelize);
const Product = ProductModel(sequelize);
const ProductVariant = ProductVariantModel(sequelize);
const Order = OrderModel(sequelize);
const OrderItem = OrderItemModel(sequelize);
const Payment = PaymentModel(sequelize);
const Shipment = ShipmentModel(sequelize);
const Invoice = InvoiceModel(sequelize);

export {
  User,
  Product,
  ProductVariant,
  Order,
  OrderItem,
  Payment,
  Shipment,
  Invoice,
};
export default sequelize;
