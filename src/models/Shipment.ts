import { Sequelize, DataTypes, Model, Optional } from "sequelize";

interface ShipmentAttributes {
  id: number;
  orderId: number;
  shipmentDate: Date;
  trackingNumber: string;
  shipmentStatus: string;
  createdAt: Date;
  updatedAt: Date;
}

interface ShipmentCreationAttributes
  extends Optional<ShipmentAttributes, "id"> {}

class Shipment
  extends Model<ShipmentAttributes, ShipmentCreationAttributes>
  implements ShipmentAttributes
{
  public id!: number;
  public orderId!: number;
  public shipmentDate!: Date;
  public trackingNumber!: string;
  public shipmentStatus!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export default function (sequelize: Sequelize): typeof Shipment {
  Shipment.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      orderId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: { model: "orders", key: "id" },
        field: "order_id",
      },
      shipmentDate: {
        type: DataTypes.DATE,
        allowNull: false,
        field: "shipment_date",
      },
      trackingNumber: {
        type: DataTypes.STRING(128),
        allowNull: false,
        field: "tracking_number",
      },
      shipmentStatus: {
        type: DataTypes.STRING(128),
        allowNull: false,
        defaultValue: "pending",
        field: "shipment_status",
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
        field: "created_at",
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
        field: "updated_at",
      },
    },
    {
      tableName: "shipments",
      sequelize,
    },
  );

  return Shipment;
}
