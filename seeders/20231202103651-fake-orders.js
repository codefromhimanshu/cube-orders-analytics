"use strict";

const { faker } = require("@faker-js/faker");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const orders = [];
    const orderItems = [];

    const maxProductVariantId = await queryInterface.rawSelect(
      "product_variants",
      {
        order: [["id", "DESC"]],
        limit: 1,
      },
      ["id"]
    );

    for (let i = 0; i < 100000; i++) {
      const orderId = i + 1;
      let totalAmount = 0;

      const itemCount = faker.number.bigInt({ min: 1, max: 5 });
      for (let j = 0; j < itemCount; j++) {
        const variantId = faker.number.bigInt({
          min: 1,
          max: maxProductVariantId,
        });

        const productId = await queryInterface.rawSelect(
          "product_variants",
          {
            where: {
              id: variantId,
            },
            limit: 1,
          },
          ["product_id"]
        );

        const productPrice = await queryInterface.rawSelect(
          "products",
          {
            where: {
              id: productId,
            },
          },
          ["price"]
        );

        const quantity = faker.number.int({ min: 1, max: 10 });
        const price = productPrice * quantity;
        totalAmount += price;

        orderItems.push({
          order_id: orderId,
          product_id: productId,
          product_variant_id: variantId,
          price: price,
          quantity: quantity,
          created_at: faker.date.between({
            from: "2016-01-01T00:00:00.000Z",
            to: "2023-12-03T00:00:00.000Z",
          }),
          updated_at: new Date(),
        });
      }

      orders.push({
        user_id: faker.number.bigInt({ min: 1, max: 10000 }),
        total_amount: totalAmount,
        status: faker.helpers.arrayElement([
          "shipped",
          "completed",
          "processing",
        ]),
        payment_status: faker.helpers.arrayElement(["paid", "partial"]),
        tax_value: faker.number.int({ min: 1, max: 20 }),
        created_at: faker.date.between({
          from: "2016-01-01T00:00:00.000Z",
          to: "2023-12-03T00:00:00.000Z",
        }),
        updated_at: new Date(),
      });
    }

    await queryInterface.bulkInsert("orders", orders, {});
    await queryInterface.bulkInsert("order_items", orderItems, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("order_items", null, {});
    await queryInterface.bulkDelete("orders", null, {});
  },
};
