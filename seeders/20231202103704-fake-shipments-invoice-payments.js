"use strict";

const { faker } = require("@faker-js/faker");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const invoices = [];
    const shipments = [];
    const payments = [];

    for (let i = 0; i < 100000; i++) {
      const orderId = faker.number.bigInt({ min: 1, max: 100000 });
      const orderTotalAmount = await queryInterface.rawSelect(
        "orders",
        {
          where: {
            id: orderId,
          },
        },
        ["total_amount"]
      );
      payments.push({
        order_id: orderId,
        amount: orderTotalAmount,
        payment_method: faker.helpers.arrayElement([
          "Credit Card",
          "PayPal",
          "Bank Transfer",
        ]),
        payment_status: faker.helpers.arrayElement([
          "paid",
          "pending",
          "failed",
        ]),
        created_at: faker.date.between({
          from: "2016-01-01T00:00:00.000Z",
          to: "2023-12-03T00:00:00.000Z",
        }),
        updated_at: new Date(),
      });

      invoices.push({
        order_id: orderId,
        issued_date: faker.date.past({
          days: 2,
          refDate: "2023-01-01T00:00:00.000Z",
        }),
        total_amount: orderTotalAmount,
        created_at: new Date(),
        updated_at: new Date(),
      });

      shipments.push({
        order_id: orderId,
        shipment_date: faker.date.past({
          days: 2,
          refDate: "2023-01-02T00:00:00.000Z",
        }),
        tracking_number: `TRACK-${faker.string.numeric(10)}`,
        shipment_status: faker.helpers.arrayElement([
          "shipped",
          "delivered",
          "pending",
        ]),
        created_at: faker.date.between({
          from: "2023-01-01T00:00:00.000Z",
          to: "2023-12-03T00:00:00.000Z",
        }),
        updated_at: new Date(),
      });
    }
    await queryInterface.bulkInsert("payments", payments, {});
    await queryInterface.bulkInsert("invoices", invoices, {});
    await queryInterface.bulkInsert("shipments", shipments, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("shipments", null, {});
    await queryInterface.bulkDelete("invoices", null, {});
    await queryInterface.bulkDelete("payments", null, {});
  },
};
