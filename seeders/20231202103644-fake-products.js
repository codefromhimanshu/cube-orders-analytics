"use strict";
const { faker } = require("@faker-js/faker");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const categories = [];
    const products = [];
    const productVariants = [];
    let productId = await queryInterface.rawSelect(
      "products",
      {
        order: [["id", "DESC"]],
        limit: 1,
      },
      ["id"]
    );
    let variantId = await queryInterface.rawSelect(
      "product_variants",
      {
        order: [["id", "DESC"]],
        limit: 1,
      },
      ["id"]
    );
    for (let k = 0; k < 20000; k++) {
      const categoryId = k + 1;
      categories.push({
        name: faker.commerce.department(),
        created_at: new Date(),
        updated_at: new Date(),
      });
      const productCount = faker.number.bigInt({ min: 1, max: 10 });
      for (let i = 0; i < productCount; i++) {
        productId++;
        products.push({
          id: productId,
          name: faker.commerce.productName(),
          category_id: categoryId,
          price: faker.commerce.price(),
          description: faker.commerce.productDescription(),
          images: [faker.image.url(), faker.image.url()],
          created_at: faker.date.between({
            from: "2016-01-01T00:00:00.000Z",
            to: "2023-12-03T00:00:00.000Z",
          }),
          updated_at: new Date(),
        });

        const variantCount = faker.number.bigInt({ min: 1, max: 5 });
        for (let j = 0; j < variantCount; j++) {
          variantId++;
          productVariants.push({
            id: variantId,
            product_id: productId,
            variant_name: faker.color.human(),
            variant_value: faker.word.noun(),
            created_at: faker.date.between({
              from: "2016-01-01T00:00:00.000Z",
              to: "2023-12-03T00:00:00.000Z",
            }),
            updated_at: new Date(),
          });
        }
      }
    }

    await queryInterface.bulkInsert("categories", categories, {});
    await queryInterface.bulkInsert("products", products, {});
    await queryInterface.bulkInsert("product_variants", productVariants, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("product_variants", null, {});
    await queryInterface.bulkDelete("products", null, {});
    await queryInterface.bulkDelete("categories", null, {});
  },
};
