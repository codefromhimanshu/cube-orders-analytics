"use strict";

const { faker } = require("@faker-js/faker");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const users = [];
    for (let i = 0; i < 10000; i++) {
      users.push({
        name: faker.person.fullName(),
        email: `${i}_${faker.internet.email()}`,
        password: faker.internet.password(),
        created_at: faker.date.between({
          from: "2016-01-01T00:00:00.000Z",
          to: "2023-12-03T00:00:00.000Z",
        }),
        updated_at: new Date(),
      });
    }
    await queryInterface.bulkInsert("users", users, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("users", null, {});
  },
};
